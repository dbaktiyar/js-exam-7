import React from 'react';
import ReactDOM from 'react-dom';
import App from './Conteiners/App';
import registerServiceWorker from "react-scripts/template/src/registerServiceWorker";

ReactDOM.render(<App />, document.getElementById('root'));
registerServiceWorker();
