import React from 'react';
import './OrderDetails.css';

export default ({ items, count, onRemove}) => {
  if (!items.some(item => count[item.id])) {
    return (
      <div className="OrderDetails">
        <h2>Order is empty!</h2>
        <h4>Please add some items!</h4>
      </div>
    )
  }
  const total = items.reduce((total, item) => {
    if (!count[item.id]) {
      return total
    }
    return count[item.id] * item.price + total
  }, 0);
  return (
    <div  className="OrderDetails">
      <h2>Order details</h2>
      <ul>
        {items.map(item => !!count[item.id] && (
          <li>
              <p className="OrderItemsName">{item.name} - </p><p>{count[item.id]} x {item.price} KGS <button onClick={e => onRemove(item)}>X</button></p>
          </li>
        ))}
        <li className="Total">Total Price: {total} KGS</li>
      </ul>
    </div>
  )
}


