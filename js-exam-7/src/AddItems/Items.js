import React from 'react';
import './Items.css';

export default ({ items, onItemClick }) => {
  return (
    <div  className="AddItems">
      <h2>Add items</h2>
      <ul>
        {items.map(item => (
          <li onClick={e => onItemClick(item)}>
            <i className="fa fa-cutlery" aria-hidden="true">

            </i>
            <p className="ItemsName">{item.name}</p>
            <p className="ItemsPrice">Price: {item.price} KGS</p>
          </li>
        ))}
      </ul>
    </div>
  )
}
