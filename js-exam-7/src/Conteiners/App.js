import React from 'react';
import OrderDetails from "../OrderDetails/OrderDetails";
import Items from "../AddItems/Items";

const ITEMS = [
    { id: 1, name: 'Hamburger', price: 80, type: 'meal' },
    { id: 2, name: 'Coffee', price: 70, type: 'drinks' },
    { id: 3, name: 'Cheeseburger', price: 90,    type: 'meal' },
    { id: 4, name: 'Tea', price: 50, type: 'drinks' },
    { id: 5, name: 'Fries', price: 45, type: 'meal' },
    { id: 6, name: 'Cola', price: 40, type: 'drinks' }
];

class App extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            count: {}
        };
        this.addItem = this.addItem.bind(this);
        this.removeItem = this.removeItem.bind(this)
    }

    addItem(item) {
        this.setState({
            count: {
                ...this.state.count,
                [item.id]: (this.state.count[item.id] ? this.state.count[item.id] : 0) + 1
            }
        })
    }

    removeItem(item) {
        this.setState({
            count: {
                ...this.state.count,
                [item.id]: this.state.count[item.id] - 1
            }
        })
    }

    render() {
        return (
            <div>
                <OrderDetails
                    items={ITEMS}
                    count={this.state.count}
                    onRemove={this.removeItem}
                />
                <Items
                    items={ITEMS}
                    onItemClick={this.addItem}
                />
            </div>
        )
    }
}


export default App;
